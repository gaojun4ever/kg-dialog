from kgdlg.modules.Encoder import EncoderRNN
from kgdlg.modules.Decoder import AttnDecoder,InputFeedDecoder,MHAttnDecoder
from kgdlg.modules.Embedding import Embedding
from kgdlg.Model import NMTModel,MoSGenerator,MemkgModel
from kgdlg.modules.Memory import MemoryEncoder
import torch
import torch.nn as nn
import kgdlg.IO as IO




def create_emb_for_encoder_and_decoder(src_vocab_size,
                                       tgt_vocab_size,
                                       src_embed_size,
                                       tgt_embed_size,
                                       padding_idx):

    embedding_encoder = Embedding(src_vocab_size,src_embed_size,padding_idx)
    embedding_decoder = Embedding(tgt_vocab_size,tgt_embed_size,padding_idx)

        
    return embedding_encoder, embedding_decoder


def create_encoder(opt):
    
    rnn_type = opt.rnn_type
    input_size = opt.embedding_size
    hidden_size = opt.hidden_size
    num_layers = opt.num_layers
    dropout = opt.dropout
    bidirectional = opt.bidirectional

    encoder = EncoderRNN(rnn_type,
                        input_size,
                        hidden_size,
                        num_layers,
                        dropout,
                        bidirectional)

    return encoder

def create_memory_encoder(vocab_size, embed_size, padding_idx):
    encoder = MemoryEncoder(vocab_size, embed_size, padding_idx)

    return encoder

def create_decoder(opt):

    decoder_type = opt.decoder_type
    rnn_type = opt.rnn_type  
    atten_model = opt.atten_model
    input_size = opt.embedding_size
    hidden_size = opt.hidden_size
    num_layers = opt.num_layers
    dropout = opt.dropout 

    if decoder_type == 'AttnDecoder':
        decoder = AttnDecoder(rnn_type,
                                atten_model,
                                input_size,
                                hidden_size,
                                num_layers,
                                dropout)
    elif decoder_type == 'InputFeedDecoder':
        decoder = InputFeedDecoder(rnn_type,
                                atten_model,
                                input_size,
                                hidden_size,
                                num_layers,
                                dropout)    

    elif decoder_type == 'MHAttnDecoder':
        decoder = MHAttnDecoder(rnn_type,
                                input_size,
                                hidden_size,
                                num_layers,
                                dropout)                                     
   

    return decoder

def create_generator(input_size, output_size):
    # generator = MoSGenerator(5, input_size, output_size)
    generator = nn.Sequential(
        nn.Linear(input_size, output_size),
        nn.LogSoftmax(dim=-1))
    return generator



def create_base_model(opt, fields):
    src_vocab_size = len(fields['src'].vocab)
    tgt_vocab_size = len(fields['tgt'].vocab)
    padding_idx = fields['src'].vocab.stoi[IO.PAD_WORD]
    enc_embedding, dec_embedding = \
            create_emb_for_encoder_and_decoder(src_vocab_size,
                                                tgt_vocab_size,
                                                opt.embedding_size,
                                                opt.embedding_size,
                                                padding_idx)
    encoder = create_encoder(opt)
    decoder = create_decoder(opt)
    mem_encoder = create_memory_encoder(src_vocab_size, opt.embedding_size, padding_idx)
    generator = create_generator(opt.hidden_size, tgt_vocab_size)
    model = MemkgModel(enc_embedding, 
                     dec_embedding,
                     mem_encoder, 
                     encoder, 
                     decoder, 
                     generator)

    # model.apply(weights_init)
    return model

