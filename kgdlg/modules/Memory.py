import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F

class MemoryEncoder(nn.Module):
    """ The standard RNN encoder. """
    def __init__(self, input_size, embedding_size, padding_idx=1):
        super(MemoryEncoder, self).__init__()
        self.padding_idx = padding_idx
        self.embedding_size = embedding_size
        self.embedding = nn.Embedding(input_size, embedding_size, padding_idx=padding_idx)
        self.weight_m = nn.Linear(embedding_size,embedding_size)
        self.weight_c = nn.Linear(embedding_size,embedding_size)
        self.sm = nn.Softmax(dim=-1)
    def score(self, h_t, h_s):
        h_s_ = h_s.transpose(1, 2)
        return torch.bmm(h_t, h_s_)
    def forward(self, input, enc_hidden):
        enc_hidden = enc_hidden.transpose(0,1)
        batch_, sourceL, dim_ = enc_hidden.size()
        embedded = self.embedding(input)
        embedded = embedded.transpose(0,1)
        m = self.weight_m(embedded)
        c = self.weight_c(embedded)
        aligh = self.score(enc_hidden, m)
        
        aligh_vector = self.sm(aligh)
        mem_hidden = torch.bmm(aligh_vector, c)
        return mem_hidden.transpose(0,1)
